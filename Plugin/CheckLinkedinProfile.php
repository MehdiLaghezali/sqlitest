<?php

namespace Training\Sqlitest\Plugin;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\RequestInterface;

class CheckLinkedinProfile
{
    /**
     * @param ResultFactory $Redirect
     * @param ManagerInterface $messageManager
     * @param RequestInterface $request
     */
    public function __construct(
        ResultFactory    $Redirect,
        ManagerInterface $messageManager,
        RequestInterface $request
    )
    {
        $this->resultFactory = $Redirect;
        $this->_messageManager = $messageManager;
        $this->getRequest = $request;
    }

    /**
     * @param \Magento\Customer\Controller\Adminhtml\Index\Save $subject
     * @param $proceed
     * @param $data
     * @param $requestInfo
     * @return mixed
     */
    public function aroundexecute(
        \Magento\Customer\Controller\Adminhtml\Index\Save $subject,
                                                          $proceed,
                                                          $data = "null",
                                                          $requestInfo = false
    )
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        // get data from the form
        $data = $subject->getRequest()->getPostValue();
        if (isset($data['customer']) || $data['customer'] != '') {
            //code to validate linkedin profile
            if (!preg_match_all("/^(http(s)?:\/\/)?([\w]+\.)?linkedin\.com\/(pub|in|profile)/m", $data['customer']['linkedin_profile'])) {
                $this->_messageManager->addError(__('Please enter a valid Linkedin Profile.'));
                return $resultRedirect->setRefererOrBaseUrl();
            }
        }
        return $proceed();
    }
}
